'use strict'

angular.module('app.plot.ctrls', [])

.controller('SearchCtrl', [
	'$scope'
	'Plot'
	'defaults'
	($scope, $plot, $defaults) ->

		$scope.results = []

		$scope.smartsearch = () ->

			if $scope.searchterms.length < 2 
				$scope.results = []
			else
				promise = $plot.retrieveData('user/search/' + $scope.searchterms)
				promise.then =>
					$scope.results = $plot.data
])

.controller('CloudCtrl', [
	'$scope'
	'Plot'
	'defaults'
	($scope, $plot, $defaults) ->

		$scope.current = $defaults.current()

		colors = ['blueOrange','summerChill','indianEarthy','firenze','retroup','doglass','yramirp']
		
		# Get random font for Danielle
		fonts = ["Avant Garde","Optima","Impact","Arial Narrow","Times New Roman","Courier New","Papyrus","Brush Script MT"]
		
		fill = d3.scale.category20b()

		svgh = parseInt(d3.select('#cloud').style('height'))
		svgw = parseInt(d3.select('#cloud').style('width'))

		# Max entries 
		$scope.limit = 1000 #Math.round(svgw / 9)

		# On small devices
		if svgw < 735
			$scope.limit = 100

		size = {width:svgw, height: Math.min(svgw,600)}

		$scope.drawD3 = (words) ->

			colorName = colors[Math.floor(Math.random()*colors.length)]

			colorList = angular.copy($defaults.colorRange[colorName])

			console.log colorName
			# First color is background
			bgcolor = colorList.shift()

			fill = d3.scale.ordinal().range(colorList);

			container = d3.select('#cloud')
		    # Create svg if it does not exist
			container.selectAll('svg').data([0])
				.enter().append("svg:svg")
					.attr(size.width)
					.attr(size.height);
			graph = container.select('svg');

			# Background
			graph.selectAll('.bg').data([0])
				.enter().append("rect")
					.attr("class", "bg")
					.attr("width", size.width)
					.attr("height", size.height);

			graph.select('.bg')
				.attr("fill", bgcolor)

			graph.attr("width", size.width)
				.attr("height", size.height)

			graph.selectAll('g').data([0])
		    	.enter().append("g")
					.attr("transform", "translate("+size.width/2+","+size.height/2+")")

			t = graph.selectAll('g').selectAll("text")
				.data(words)

			duration = 10

			t.enter().append("text")
				.style("opacity", 0)

			t.style("fill", (d, i) -> return fill(i))
				.attr("text-anchor", "middle")
				.text((d) -> return d.text)
				.style("font-family", $scope.font)

			t.transition().duration(750).delay((d, i) -> return i * duration)
				.style("font-size", (d) -> return d.size + "px" )
				.style("opacity", 1)
				.attr("transform", (d) ->
					return "translate(" + [d.x, d.y] + ")";
				)

			t.exit().transition().duration(750)
				.style("opacity", 0)
				.remove()
		$scope.top = ->
			if $scope.entries == $scope.limit
				return 'top '
		$scope.render = (args) ->
			args.limit = $scope.limit
			$scope.font = fonts[Math.floor(Math.random()*fonts.length)]
			promise = $plot.retrieveData('user/get_traits', args)
			promise.then =>
				$scope.entries = $plot.data.length
				max = d3.max($plot.data, (d) -> return d.size * d.text.length)
				max = (max * 1.0) / svgw
				data = $plot.data.map( (d) -> return {text: d.text, size: Math.round(d.size / max)})
				d3.layout.cloud().size([size.width, size.height])
					.words(data)
					.padding(1)
					.rotate(-> return 0)
					.font($scope.font)
					.timeInterval(1)
					.fontSize((d) -> return d.size)
					.on("end", -> $scope.drawD3(data))
					.start();

		# Get current level and trait
		args = {}
		if $scope.current.level != 'none'
			args[$scope.current.level] = $scope.current.trait;
		$scope.render(args)

		# Listen to broadcast
		$scope.$on('levels.changed', (e, args) ->
			$scope.render(args)
		)

		# Listen to destroy
		$scope.$on('$destroy', ->
			#pubsubService.off('myEvent');
		)




])

.controller('ColCtrl', [
	'$scope'
	'Plot'
	'defaults'
	($scope, $plot, $defaults) ->

		$scope.level = $defaults.levels[0]

		$scope.draw = (id, kind) ->
			opts = {
				barWidth: 30
				margin: {top: 20, right: 10, bottom: 100, left: 50}
				colors: ['#2484c1']
				tofixed: 0
			}
			if kind
				graphId = '#' + id + '-' + kind
				kind = '/' + kind
			else
				graphId = '#' + id
				kind = ''

			promise = $plot.retrieveData('overview/trait_count/' + id + kind)
			promise.then =>
				graph = displayColumnGraph(graphId , $plot.data, opts)
])

.controller('PieCtrl', [
	'$scope'
	'Plot'
	'defaults'
	($scope, $plot, $defaults) ->

		$scope.levels = $defaults.levels
		$scope.draw = (id, params) ->
			promise = $plot.retrieveData('overview/pie/'+id, params)
			promise.then => 
				$scope.pie = new d3pie(id, {
					size:
						pieOuterRadius: 80,
						canvasHeight: 300
					data:
						content: $plot.data
						smallSegmentGrouping:
							enabled: false
							value: 2
						sortOrder: 'label-asc'
					labels:
						mainLabel:
							fontSize: 13
						percentage:
							fontSize: 13
						inner:
							hideWhenLessThanPercentage: 5
					tooltips:
						enabled: true
						type: 'placeholder'
						string: "{label}, {value}, {percentage}%"
				})
])

.controller('PlotCtrl', [
	'$scope'
	'Plot'
	'defaults'
	'$compile'
	($scope, $plot, $defaults, $compile) ->

		$scope.current = $defaults.current()
		
		$scope.mainpanels = [
			{id: "graph1", tableid: "stats1", dropdownid: 'dropdown1', title: 'Twin correlations'}
			{id: "graph2a", tableid: "stats2a", dropdownid: 'dropdown3', title: 'Least Squares Estimates'}
			{id: "graph2", tableid: "stats2", dropdownid: 'dropdown2', title: 'Reported ACE'}
		]


		$scope.estimates =  [
			{
				title: $scope.currentName,
				names: $defaults.defaultEstimates,
				id:'#graph1',
				url: "user/meta",
				table: {id: "#stats1"},
				colors: $defaults.colorRange.blue
			},
			{
				title: $scope.currentName,
				names: $defaults.defaultACE,
				id:'#graph2',
				url: "user/meta",
				table: {id: "#stats2"},
				colors: $defaults.colorRange.red
			},
			{
				title: $scope.currentName,
				names: $defaults.defaultACE.concat($defaults.defaultEstimates),
				id:'#graph2a',
				url: "user/lsh2",
				table: {id: "#stats2a"},
				colors: $defaults.colorRange.green
			}
		]

		$scope.render = (args) ->
			for estimate in $scope.estimates
				do (estimate) ->
					params = angular.copy(args)
					params['estimates[]'] = estimate.names
					promise = $plot.retrieveData(estimate.url, params)
					promise.then => 
						opts = {
							title: '',
							margin:{top: 0, right: 10, bottom: 20, left: 65},
							colors: estimate.colors
						}
						graph = displayBarGraph(estimate.id, $plot.data, opts)
						if estimate.table.id
							$scope.params = params;
							tabulate(estimate.table.id, $plot.data, $defaults.columns)
							$compile($('*[tooltip-html-unsafe]'))($scope)

		# Get current level and trait
		args = {}
		if $scope.current.level != 'none'
			args[$scope.current.level] = $scope.current.trait;
		$scope.render(args)

		# Listen to broadcast
		$scope.$on('levels.changed', (e, args) ->
			$scope.render(args)
		)

])

.controller('pi0Ctrl', [
	'$scope'
	'Plot'
	'defaults'
	($scope, $plot, $defaults) ->

		$scope.levelselect = $defaults.levels[0]
		$scope.levelselectors = $defaults.levels

		$scope.new_table = (p) ->
			$scope.levelselect = p;
			promise = $plot.retrieveData('overview/pi0_all/' + p.value, {})
			promise.then =>
				$defaults.current().filename = 'pi0 '+ p.label
				$scope.tabledata = $plot.data;


])

.controller('multiTraitCtrl', [
	'$scope'
	'$location'
	'Plot'
	'defaults'
	($scope, $location, $plot, $defaults) ->

		if $location.path() == '/multiple/reported_ace'
			$scope.groups = $defaults.defaultACE
			$scope.view = 'ace'
			$scope.cfilter = {h2_all :true, c2_all: true}
			$scope.title = 'Reported ACE'
			$scope.template = 'myModalACE.html'
			$scope.prefix = ''
			path = 'levels/level_h2/'
			colors = $defaults.colorRange.ace
		else if $location.path() == '/multiple/twin_cor'
			$scope.groups = $defaults.defaultEstimates
			$scope.view = 'cor'
			$scope.cfilter = {mzall :true, dzall: true}
			$scope.title = 'Twin Correlations'
			$scope.template = 'myModalCor.html'
			$scope.prefix = 'r'
			path = 'levels/level_cor/'
			colors = $defaults.colorRange.head
		else
			$scope.groups = $defaults.lseLabels
			$scope.view = 'lse'
			$scope.cfilter = {'2(mz-dz) all' :true, '2dz-mz all': true}
			$scope.title = 'Least Squares Estimates'
			$scope.template = 'myModalLse.html'
			$scope.prefix = ''
			path = 'levels/level_lse/'
			colors = $defaults.colorRange.cor


		$scope.new_barplot = ->

			promise = $plot.retrieveData(path + $scope.levelselect, {})
			promise.then =>
				$scope.update_barplot()

		$scope.update_barplot = ->

			opts = {sortcol: $scope.groups.indexOf($scope.grselect)}
			opts.hide = [];
			opts.colors = colors
			i = 0
			c = d3.scale.ordinal()
				.range(opts.colors);

			# Collect checkboxes
			d3.selectAll('input[type="checkbox"]').each( ->
				# Color them appropriately
				d3.select(@parentNode).select('span').style('background-color', ->
					return c(i++))
				# Hide unchecked
				@checked || opts.hide.push(@value)
			)
			punchcard_3('#barplot', $plot.data.data, opts)

])

.controller('punchCardCtrl', [
	'$scope'
	'$location'
	'Plot'
	'defaults'
	($scope, $location, $plot, $defaults) ->

		if $location.path() == '/multiple/punchcard2'
			$scope.groups=['h2_all', 'h2_ss', 'h2_m', 'h2_f', 'c2_all', 'c2_ss', 'c2_m', 'c2_f']
			$scope.cfilter = {h2_all :true, c2_all: true}
			path = 'user/levelh2/'
			colors = $defaults.colorRange.ace
		else
			$scope.groups=['mzall', 'mzm', 'mzf', 'dzall', 'dzss', 'dzm', 'dzf', 'dos']
			$scope.cfilter = {mzall :true, dzall: true}
			path = 'user/level/'
			colors = $defaults.colorRange.cor

		$scope.new_punchcard = ->

			promise = $plot.retrieveData(path + $scope.levelselect, {})
			promise.then =>
				$scope.update_punchcard()

		$scope.update_punchcard =  ->

			opts = {sortcol: $scope.groups.indexOf($scope.grselect)}
			opts.hide = [];
			opts.colors = colors
			i = 0
			c = d3.scale.ordinal()
				.range(opts.colors);

			# Collect checkboxes
			d3.selectAll('input[type="checkbox"]').each( ->
				# Color them appropriately
				d3.select(@parentNode).select('span').style('background-color', ->
					return c(i++))
				# Hide unchecked
				@checked || opts.hide.push(@value)
			)
			punchcard_2('#levels', $plot.data.data, opts)

])

.controller('authorCtrl', [
	'$scope'
	'Plot'
	($scope, $plot) ->

		chart = {}

		$scope.items = [
			{id: 'group', name: 'Order by Cluster'}
			{id: 'name', name: 'Order by Name'}
			{id: 'count', name: 'Order by Frequency'}
		]

		# Initial value
		$scope.order = $scope.items[0].name

		$scope.setOrder = (order) ->
			$scope.order = order.name
			chart.sort(order.id)

		params = {}
		width = $('#matrix').width()
		minpapers = Math.round(6 + 20000 / parseInt(width) )

		promise = $plot.retrieveData('user/authors/' + minpapers, params)
		promise.then =>
			# Initialize chart
			chart = matrix()
				.minpapers(minpapers)
			# Show chart
			d3.select('#matrix')
				.datum($plot.data)
				.call(chart)


])

.controller('journalCtrl', [
	'$scope'
	'Plot'
	($scope, $plot) ->

		params = {}

		promise = $plot.retrieveData('user/journal', params)
		promise.then =>
			punchcard('#journal', $plot.data)

])

.controller('mapCtrl', [
	'$scope'
	'Plot'
	'defaults'
	($scope, $plot, $defaults) ->

		$scope.current = $defaults.current()

		$scope.corrs = $defaults.defaultEstimates
		$scope.aces = $defaults.defaultACE

		# Set initial mapData
		$scope.mapData = 
			entries: {}
			country: {}

		# Show map
		$('.map').vectorMap(
			regionStyle:
				initial:
					fill: '#cccccc'
			zoomOnScroll: false
			series:
				regions: [
					values: $scope.mapData.entries
					scale: ['#C8EEFF', '#0071A4']
					min: 0
					max: 5000
					normalizeFunction: 'polynomial'
					attribute: 'fill'
				]
					
			onRegionClick: (e, code) ->
				$('.map').vectorMap('set', 'focus', {region: code, animate: true})
			onRegionTipShow: (e, el, code) ->
				if $scope.mapData.country[code]
					str = ' (Papers - '+$scope.mapData.country[code]+')'
				else
					str = ' (No Papers)'

				if $scope.mapData.entries[code]
					str = ' (Ntraits - '+$scope.mapData.entries[code]+')'
				else
					str = ' (No Data)'

				el.html(el.html() + str);

				   
		)

		# Get mapdata
		$scope.render = (args) ->
			params = angular.copy(args)
			promise = $plot.retrieveData("user/country", params)
			promise.then =>
				# Check if map still exists
				if $('.map').length
					$scope.current.filename = 'country ' + $scope.current.trait
					$scope.mapData = $plot.data
					$scope.mapObj = $('.map').vectorMap('get', 'mapObject')
					$scope.mapObj.reset()
					$scope.mapObj.series.regions[0].setValues($plot.data.entries)

		# Reset button
		$scope.reset = ->
			$('.map').vectorMap('set', 'focus', {scale: 1, x: 0.5, y: 0.5, animate: true})

		# Get current level and trait
		args = {}
		if $scope.current.level != 'none'
			args[$scope.current.level] = $scope.current.trait;
		$scope.render(args)

		# Listen to broadcast
		$scope.$on('levels.changed', (e, args) ->
			$scope.render(args)
		)

])

.controller('Scatter1Ctrl', [
	'$scope'
	'Plot'
	'defaults'
	'$compile'
	($scope, $plot, $defaults, $compile) ->

		$scope.current = $defaults.current()

		$scope.estimates = [
			{
				names:['mzall', 'dzall'],
				id:'graph5',
				table: {id: "stats5"},
				url: "user/scatter"
			},
			{
				names:['mzall', 'dzss'],
				id:'graph6',
				table: {id: "stats6"},
				url: "user/scatter"
			},
			{
				names:['mzm', 'dzm'],
				id:'graph7',
				table: {id: "stats7"},
				url: "user/scatter"
			},
			{
				names:['mzf', 'dzf'],
				id:'graph8',
				table: {id: "stats8"},
				url: "user/scatter"
			}
		]

		columnsScatter = [
			{name:'n_entries', display: 'Ntraits', align: 'right', title: 'Number of entries on which the<br>estimate is based.'},
			{name: 'fixedB_h', display: 'π₀(h)', align: 'right', fixed: 4, missing: -1, title: "Proportion of observations<br>consistent with<br>H₀: 2*(rMZ-rDZ) = 0"},
			{name: 'fixedB_c', display: 'π₀(c)', align: 'right', fixed: 4, missing: -1, title: "Proportion of observations<br>consistent with<br>H₀: 2*rDZ-rMZ = 0"},
		]

		$scope.render = (args) ->
			for estimate in $scope.estimates
				do (estimate) ->
					params = angular.copy(args)
					params['estimates[]'] = estimate.names;
					promise = $plot.retrieveData(estimate.url, params)
					promise.then =>
						opts = {}
						opts.title = ''
						opts.label = {
							xaxis: estimate.names[0],
							yaxis: estimate.names[1]
						}
						n_entries = $plot.data.length
						# console.log($plot.data)
						graph = displayHexbinGraph('#'+estimate.id, $plot.data, opts)

						if estimate.table.id
							# Get pi0
							promise = $plot.retrieveData('user/pi0', params)
							promise.then =>
								tabulate('#'+estimate.table.id, [$plot.data], columnsScatter)
								$compile($('*[tooltip-html-unsafe]'))($scope)

		# Get current level and trait
		args = {}
		if $scope.current.level != 'none'
			args[$scope.current.level] = $scope.current.trait;
		$scope.render(args)

		# Listen to broadcast
		$scope.$on('levels.changed', (e, args) ->
			$scope.render(args)
		)

])

.controller('Scatter2Ctrl', [
	'$scope'
	'Plot'
	'defaults'
	($scope, $plot, $defaults) ->

		$scope.current = $defaults.current()

		$scope.estimates = [
			{name:'mzall', id:'scat1', url: "user/scatter_eff_sam"},
			{name:'mzm', id:'scat2', url: "user/scatter_eff_sam"},
			{name:'mzf', id:'scat3', url: "user/scatter_eff_sam"},
			{name:'dzall', id:'scat4', url: "user/scatter_eff_sam"},
			{name:'dzss', id:'scat5', url: "user/scatter_eff_sam"},
			{name:'dzm', id:'scat6', url: "user/scatter_eff_sam"},
			{name:'dzf', id:'scat7', url: "user/scatter_eff_sam"},
			{name:'dos', id:'scat8', url: "user/scatter_eff_sam"},
			{name:'h2_all', id:'scat9', url: "user/scatter_eff_sam"},
			{name:'h2_ss', id:'scat10', url: "user/scatter_eff_sam"},
			{name:'h2_m', id:'scat11', url: "user/scatter_eff_sam"},
			{name:'h2_f',id:'scat12', url: "user/scatter_eff_sam"},
			{name:'c2_all', id:'scat13', url: "user/scatter_eff_sam"},
			{name:'c2_ss', id:'scat14', url: "user/scatter_eff_sam"},
			{name:'c2_m', id:'scat15', url: "user/scatter_eff_sam"},
			{name:'c2_f', id:'scat16', url: "user/scatter_eff_sam"}
		]

		$scope.render = (args) ->
			for estimate in $scope.estimates
				do (estimate) ->
					params = angular.copy(args)
					params['estimates[]'] = estimate.name;
					promise = $plot.retrieveData(estimate.url, params)
					promise.then =>
						opts = {}
						opts.title = ''
						opts.label = {xaxis: 'log10(npairs)', yaxis: 'r'}
						opts.domain = {x:[0,4]}
						# console.log($plot.data)
						graph = displayScatterGraph('#'+estimate.id, $plot.data, opts)

		# Get current level and trait
		args = {}
		if $scope.current.level != 'none'
			args[$scope.current.level] = $scope.current.trait;
		$scope.render(args)

		# Listen to broadcast
		$scope.$on('levels.changed', (e, args) ->
			$scope.render(args)
		)
])

.controller('AgePlotCtrl', [
	"$scope"
	"Plot"
	"defaults"
	"$compile"
	($scope, $plot, $defaults, $compile) ->

		$scope.agegroups = [
			{number:0, name: 'Age 0-11'}
			{number:1, name: 'Age 12-17'}
			{number:2, name: 'Age 18-64'}
			{number:3, name: 'Age 65+'}
		];

		$scope.current = $defaults.current()

		estimates = []

		for group in $scope.agegroups
			do (group) ->
				estimates.push(
					{
						title: group.name,
						names: $defaults.defaultEstimates,
						id: '#cor-age-group-' + group.number,
						url: "user/meta",
						table: {id: "#stats-cor-group-" + group.number},
						colors: $defaults.colorRange.blue,
						agecohort: group.number
					}
				)

		for group in $scope.agegroups
			do (group) ->
				estimates.push(
					{
						title: group.name,
						names: $defaults.defaultACE.concat($defaults.defaultEstimates),
						id: '#m2-age-group-' + group.number,
						url: "user/lsh2",
						table: {id: "#stats-m2-group-" + group.number},
						colors: $defaults.colorRange.green,
						agecohort: group.number
					}
				)

		for group in $scope.agegroups
			do (group) ->
				estimates.push(
					{
						title: group.name,
						names: $defaults.defaultACE,
						id: '#ace-age-group-' + group.number,
						url: "user/meta",
						table: {id: "#stats-ace-group-" + group.number},
						colors: $defaults.colorRange.red,
						agecohort: group.number
					}
				)
		$scope.render = (args) ->
			for estimate in estimates
				do (estimate) ->
					params = angular.copy(args)
					params.agecohort = estimate.agecohort
					params['estimates[]'] = estimate.names
					promise = $plot.retrieveData(estimate.url, params)
					promise.then => 
						opts = {
							title: '',
							margin:{top: 0, right: 10, bottom: 20, left: 65},
							colors: estimate.colors
						}
						graph = displayBarGraph(estimate.id, $plot.data, opts)
						if estimate.table.id
							$scope.params = params;
							tabulate(estimate.table.id, $plot.data, $defaults.columns)
							$compile($('*[tooltip-html-unsafe]'))($scope)

		# Get current level and trait
		args = {}
		if $scope.current.level != 'none'
			args[$scope.current.level] = $scope.current.trait;
		$scope.render(args)

		# Listen to broadcast
		$scope.$on('levels.changed', (e, args) ->
			$scope.render(args)
		)

])

.factory 'Plot', [
	'$http'
	'defaults'
	($http, $defaults) ->
		new class Plot
			data:[]
			constructor: ->
				@timestamp = Date.now() - 900000

			retrieveData: (url, params) ->
				request = $http.get $defaults.baseurl + url, params: params
				request.then (result) =>
					@data = result.data

]