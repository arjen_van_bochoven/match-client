'use strict'

angular.module('app.map.directives', [])

.directive('map', [ ->
    return {
        restrict: 'A'
        link: (scope, element, attrs) ->
           scope.$watch("mapData", (n,o) ->

                # Remove tooltip
                $('div.jvectormap-tip').remove()

                # Rebuild map (fix this)
                element.empty()
                $(element).width('auto')
                $(element).height(550)

                scope.map = $(element).vectorMap({
                    backgroundColor: 'transparent'
                    regionStyle: { 
                      initial: {
                        fill: '#cccccc'
                      }
                    },
                    map: 'world_mill_en',
                    zoomOnScroll: false,
                    series: {
                        regions: [{ 
                            values: scope.mapData.entries,
                            scale: ['#C8EEFF', '#0071A4'],
                            normalizeFunction: 'polynomial'
                        }]
                    },
                    onRegionClick: (e, code) ->
                        $('.map').vectorMap('set', 'focus', {region: code, animate: true})
                    onRegionTipShow: (e, el, code) ->

                        if scope.mapData.country[code]
                          str = ' (Papers - '+scope.mapData.country[code]+')'
                        else
                          str = ' (No Papers)'

                        if scope.mapData.entries[code]
                            str = ' (Ntraits - '+scope.mapData.entries[code]+')'
                        else
                            str = ' (No Data)'

                        el.html(el.html() + str);

                })  
            )
    }
])