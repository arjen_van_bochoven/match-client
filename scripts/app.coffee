'use strict';

angular.module('app', [
    # Angular modules
    'ngRoute'
    'ngAnimate'

    # 3rd Party Modules
    'ui.bootstrap'
    #'easypiechart'
    #'mgo-angular-wizard'
    #'textAngular'

    # Custom modules
    'app.ui.ctrls'
    'app.ui.directives'
    'app.ui.services'
    'app.controllers'
    'app.directives'
    #'app.form.validation'
    #'app.ui.form.ctrls'
    #'app.ui.form.directives'
    'app.tables'
    'app.task'
    'app.localization'
    #'app.chart.ctrls'
    #'app.chart.directives'
    #'app.map.ctrls'
    #'app.map.directives'
    'app.plot.ctrls'
])
    
.config([
    '$routeProvider'
    ($routeProvider) ->
        $routeProvider

            # Home
            .when(
                '/'
                redirectTo: '/home'
            )
            .when(
                '/home'
                templateUrl: 'views/home.html'
            )

            # About
            .when(
                '/about'
                templateUrl: 'views/pages/about.html'
            )

            # Overview
            .when(
                '/overview'
                redirectTo: '/overview/authors'
            )

            .when(
                '/overview/authors'
                templateUrl: 'views/overview/authors.html'
            )

            .when(
                '/overview/journal'
                templateUrl: 'views/overview/journal.html'
            )

            .when(
                '/overview/glossary'
                templateUrl: 'views/overview/glossary.html'
            )

            .when(
                '/overview/search'
                templateUrl: 'views/plots/search.html'
            )



            # Specific
            .when(
                '/specific'
                redirectTo: '/specific/plot1'
            )

            .when(
                '/specific/plot1'
                templateUrl: 'views/plots/plot1.html'
            )

            .when(
                '/specific/cloud'
                templateUrl: 'views/plots/cloud.html'
            )

            .when(
                '/specific/age_groups'
                templateUrl: 'views/plots/age_groups.html'
            )

            .when(
                '/specific/scatter_1'
                templateUrl: 'views/plots/scatter_1.html'
            )

            .when(
                '/specific/scatter_2'
                templateUrl: 'views/plots/scatter_2.html'
            )

            .when(
                '/specific/country'
                templateUrl: 'views/maps/worldmap.html'
            )

            # Multiple
            .when(
                '/multiple'
                redirectTo: '/multiple/twin_cor'
            )

            .when(
                '/multiple/reported_ace'
                templateUrl: 'views/plots/multi_traits.html'
            )

            .when(
                '/multiple/twin_cor'
                templateUrl: 'views/plots/multi_traits.html'
            )


            .when(
                '/multiple/lse'
                templateUrl: 'views/plots/multi_traits.html'
            )

            .when(
                '/multiple/pi0'
                templateUrl: 'views/overview/pi0_all.html'
            )
            .when(
                '/overview/traits'
                templateUrl: 'views/plots/traits.html'
            )
            .when(
                '/overview/pub_years'
                templateUrl: 'views/overview/pub_years.html'
            )

            # Utility pages
 
            .when(
                '/404'
                templateUrl: 'views/pages/404.html'
            )
            .when(
                '/pages/500'
                templateUrl: 'views/pages/500.html'
            )
            

            .otherwise(
                redirectTo: '/404'
            )
])


