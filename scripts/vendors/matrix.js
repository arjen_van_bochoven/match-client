// Basic code by Mike Bostock
function matrix()
{
	var svgw = 600,
	    margin = {top: 120, right: 10, bottom: 10, left: 120},
	    width = svgw - margin.left - margin.right,
	    height = svgw - margin.top - margin.bottom,
	    minpapers = 0,
	    order = 'group',
	    orders = {},
	    graph = {},
	    x = d3.scale.ordinal().rangeBands([0, width]),
	    z = d3.scale.linear().domain([0, 78]).clamp(true),
	    c = d3.scale.category10().domain(d3.range(10));

	function chart(selection) {
		selection.each(function(authors) {

			svgw = parseInt(selection.style('width'));
			width = svgw - margin.left - margin.right;
			height = svgw - margin.top - margin.bottom;
			x = d3.scale.ordinal().rangeBands([0, width]);

			// Create svg if it does not exist
			selection.selectAll('svg').data([0])
				.enter().append("svg:svg");

			graph = selection.select('svg')
							.attr("width", svgw)
						    .attr("height", svgw);


			var svg = graph.append("g")
			    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			// Add label
			graph.append("g")
				    .attr("transform", "translate(10, 20)")
					.append("text")
						.text( "Filter: min. " + minpapers + " papers")

			var matrix = [],
			  nodes = authors.nodes,
			  n = nodes.length;

			// Compute index per node.
			nodes.forEach(function(node, i) {
				node.index = i;
				node.count = 0;
				matrix[i] = d3.range(n).map(function(j) { return {x: j, y: i, z: 0}; });
			});

			// Convert links to matrix; count total collaborations.
			authors.links.forEach(function(link) {
				matrix[link.source][link.target].z += link.value;
				matrix[link.target][link.source].z += link.value;
				matrix[link.source][link.source].z += link.value;
				matrix[link.target][link.target].z += link.value;
				nodes[link.source].count += link.value;
				nodes[link.target].count += link.value;
			});

			// Precompute the orders.
			orders = {
				name: d3.range(n).sort(function(a, b) { return d3.ascending(nodes[a].name, nodes[b].name); }),
				count: d3.range(n).sort(function(a, b) { return nodes[b].count - nodes[a].count; }),
				group: d3.range(n).sort(function(a, b) { return nodes[a].group - nodes[b].group; })
			};

			// The default sort order.
			x.domain(orders[order]);

			svg.append("rect")
			  .attr("class", "background")
			  .attr("width", width)
			  .attr("height", height);

			var row = svg.selectAll(".row")
			  .data(matrix)
			.enter().append("g")
			  .attr("class", "row")
			  .attr("transform", function(d, i) { return "translate(0," + x(i) + ")"; })
			  .each(row);

			row.append("line")
			  .attr("x2", width);

			row.append("text")
			  .attr("x", -6)
			  .attr("y", x.rangeBand() / 2)
			  .attr("dy", ".32em")
			  .attr("text-anchor", "end")
			  .text(function(d, i) { return nodes[i].name; });

			var column = svg.selectAll(".column")
			  .data(matrix)
			.enter().append("g")
			  .attr("class", "column")
			  .attr("transform", function(d, i) { return "translate(" + x(i) + ")rotate(-90)"; });

			column.append("line")
			  .attr("x1", -width);

			column.append("text")
			  .attr("x", 6)
			  .attr("y", x.rangeBand() / 2)
			  .attr("dy", ".32em")
			  .attr("text-anchor", "start")
			  .text(function(d, i) { return nodes[i].name; });

			function row(row) {
				var cell = d3.select(this).selectAll(".cell")
				    .data(row.filter(function(d) { return d.z; }))
				  .enter().append("rect")
				    .attr("class", "cell")
				    .attr("x", function(d) { return x(d.x); })
				    .attr("width", x.rangeBand())
				    .attr("height", x.rangeBand())
				    .style("fill-opacity", function(d) { return z(d.z); })
				    .style("fill", function(d) { return nodes[d.x].group == nodes[d.y].group ? c(nodes[d.x].group) : null; })
				    .on("mouseover", mouseover)
				    .on("mouseout", mouseout);
			}

			function mouseover(p) {
				d3.selectAll(".row text").classed("active", function(d, i) { return i == p.y; });
				d3.selectAll(".column text").classed("active", function(d, i) { return i == p.x; });
			}

			function mouseout() {
				d3.selectAll("text").classed("active", false);
			}

		});
	};

	chart.sort = function(value) {
		x.domain(orders[value]);

		var t = graph.transition().duration(2500);

		t.selectAll(".row")
		    .delay(function(d, i) { return x(i) * 4; })
		    .attr("transform", function(d, i) { return "translate(0," + x(i) + ")"; })
		  .selectAll(".cell")
		    .delay(function(d) { return x(d.x) * 4; })
		    .attr("x", function(d) { return x(d.x); });

		t.selectAll(".column")
		    .delay(function(d, i) { return x(i) * 4; })
		    .attr("transform", function(d, i) { return "translate(" + x(i) + ")rotate(-90)"; });
	}

	chart.minpapers = function(_) {
		if (!arguments.length) return minpapers;
		minpapers = _;
		return chart;
	};

	chart.order = function(_) {
		if (!arguments.length) return order;
		order = _;
		return chart;
	};

	return chart;

}



//*******************************************************

function timeSeriesChart() {
  var margin = {top: 20, right: 20, bottom: 20, left: 20},
      width = 760,
      height = 120,
      xValue = function(d) { return d[0]; },
      yValue = function(d) { return d[1]; },
      xScale = d3.time.scale(),
      yScale = d3.scale.linear(),
      xAxis = d3.svg.axis().scale(xScale).orient("bottom").tickSize(6, 0),
      area = d3.svg.area().x(X).y1(Y),
      line = d3.svg.line().x(X).y(Y);

  function chart(selection) {
    selection.each(function(data) {

      // Convert data to standard representation greedily;
      // this is needed for nondeterministic accessors.
      data = data.map(function(d, i) {
        return [xValue.call(data, d, i), yValue.call(data, d, i)];
      });

      // Update the x-scale.
      xScale
          .domain(d3.extent(data, function(d) { return d[0]; }))
          .range([0, width - margin.left - margin.right]);

      // Update the y-scale.
      yScale
          .domain([0, d3.max(data, function(d) { return d[1]; })])
          .range([height - margin.top - margin.bottom, 0]);

      // Select the svg element, if it exists.
      var svg = d3.select(this).selectAll("svg").data([data]);

      // Otherwise, create the skeletal chart.
      var gEnter = svg.enter().append("svg").append("g");
      gEnter.append("path").attr("class", "area");
      gEnter.append("path").attr("class", "line");
      gEnter.append("g").attr("class", "x axis");

      // Update the outer dimensions.
      svg .attr("width", width)
          .attr("height", height);

      // Update the inner dimensions.
      var g = svg.select("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // Update the area path.
      g.select(".area")
          .attr("d", area.y0(yScale.range()[0]));

      // Update the line path.
      g.select(".line")
          .attr("d", line);

      // Update the x-axis.
      g.select(".x.axis")
          .attr("transform", "translate(0," + yScale.range()[0] + ")")
          .call(xAxis);
    });
  }

  // The x-accessor for the path generator; xScale ∘ xValue.
  function X(d) {
    return xScale(d[0]);
  }

  // The x-accessor for the path generator; yScale ∘ yValue.
  function Y(d) {
    return yScale(d[1]);
  }

  chart.margin = function(_) {
    if (!arguments.length) return margin;
    margin = _;
    return chart;
  };

  chart.width = function(_) {
    if (!arguments.length) return width;
    width = _;
    return chart;
  };

  chart.height = function(_) {
    if (!arguments.length) return height;
    height = _;
    return chart;
  };

  chart.x = function(_) {
    if (!arguments.length) return xValue;
    xValue = _;
    return chart;
  };

  chart.y = function(_) {
    if (!arguments.length) return yValue;
    yValue = _;
    return chart;
  };

  return chart;
}

