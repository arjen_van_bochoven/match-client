/// DISPLAY COLUMN GRAPH ///

function displayColumnGraph(id, data, opts)
{
	// Get to the container object
	var container = d3.select(id);

	if(container.empty()) return;

	opts = opts || container.select('div.options').datum();

	data = data || [];

	// Get minimum xvalue
	if(data.length)
	{
		var ymin = d3.min(data, function(d){ return d.value < 0 ? d.value - d.se : 0 });
		var ymax = d3.max(data, function(d){ return d.value + d.se > 1 ? d.value + d.se : 1 });
		opts.ydomain = [ymin, ymax];
	}

	
	// Init options
	opts.margin = opts.margin || {top: 30, right: 10, bottom: 20, left: 35};
	opts.colors = opts.colors || ["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"];
	opts.ydomain = opts.ydomain || [0,1];
	opts.tofixed = opts.tofixed == undefined ? 2 : opts.tofixed;

	 // Handle resize
    opts.registeredForResize = opts.registeredForResize || false;
    var graphResize = 0;

    if( ! opts.registeredForResize)
    {
		
		$(window).resize(function(e) {

			graphResize = setTimeout(function(){displayColumnGraph(id)}, 200);

		});

		opts.registeredForResize = true;
    }
    


	// Create options store
	container.selectAll('div.options').data([0])
    	.enter().append("div")
			.attr("class", "options")

	var svgh = parseInt(d3.select(id).style('height'), 10),
		svgw = parseInt(d3.select(id).style('width'), 10),
		margin = opts.margin;

	var	width = svgw - margin.left - margin.right,
		height = svgh - margin.top - margin.bottom
	

	if(opts.barWidth)
	{
		width = opts.barWidth * data.length;
		svgw = width + margin.left + margin.right
	}
	else
	{
		svgw = "100%"
	}
		

	opts.barWidth = opts.barWidth || width / data.length;

	var color = d3.scale.ordinal()
	    .range(opts.colors);

	// Store options
	container.select('div.options').data([opts]);

	// Create svg if it does not exist
    container.selectAll('svg').data([0])
    	.enter().append("svg:svg")
					.attr("width", svgw)
					.attr("height", svgh);
	var graph = container.select('svg');


	// yAxis
	var y = d3.scale.linear()
		.range([height, 0])
		.domain(opts.ydomain);
    var yAxis = d3.svg.axis()
    	.scale(y)
    	.orient("left")
    	.ticks(Math.max(height/30, 2));
	graph.selectAll('.y.axis').data([0])
    	.enter().append("g")
		  .attr("class", "y axis")
		  .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
		  .call(yAxis);

	graph.selectAll('.y.axis').transition().duration(750).call(yAxis);

	// Grid
	yAxis.tickSize(width)
		.tickFormat('')
	graph.selectAll('.y.grid').data([0])
    	.enter().append("g")
		  .attr("class", "y grid")
			  .attr("transform", "translate(" + (margin.left + width) + "," + margin.top + ")")
		  .call(yAxis);

	//graph.selectAll('.y.grid').transition().duration(750).call(yAxis);


	// Bar group
	var bars = graph.selectAll(".bar")
	    .data(data);

	var g = bars.enter().append("g")
		.attr("class", "bar")
	    .attr("transform", function(d, i) { return "translate(" + (i * opts.barWidth + margin.left) + "," + margin.top + ")"; })
	
	// Bar area
	g.append("rect")
	    .attr("height", 0) // Initial height = 0
	    .attr("y", y(0))
	    .attr("class", "area")
	    .attr("width", opts.barWidth - 1)
	    .attr("rx", 2)
		.attr("ry", 2)
	    .style("fill", function(d) { return color(d.name); });

	t = bars.transition().duration(750);

	// Update bar area rect
	t.select(".area")
		.attr("height", function(d){ return Math.abs(y(d.value) - y(0))})
		.attr("y", function(d) { return d.value < 0 ? y(d.value) :y(d.value) })

	// xAxis
	var x = d3.scale.ordinal()
				.rangeRoundBands([0, width])
				.domain(data.map(function(d) { return d.name; }));
    var xAxis = d3.svg.axis().scale(x).orient("bottom");
	graph.selectAll('.x.axis').data([0])
    	.enter().append("g")
		  .attr("class", "x axis")
		  .attr("transform", "translate(" + margin.left + "," + (margin.top + height) + ")")
		  .call(xAxis)
		  .selectAll('text')
			.style("text-anchor", "end")
			.attr("dx", "-.8em")
			.attr("dy", ".15em")
			.attr("transform", "rotate(-45)")


	return graph;
}