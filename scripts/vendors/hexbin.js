function displayHexbinGraph(id, data, opts)
{
		data = data || [];
		opts = opts || displayScatterGraph.opts || {};

		xmin = 0;
		ymin = 0;

		// Get minimum xvalue
		if(data.length)
		{
			xmin = d3.min(data, function(d){ return d.x < 0 ? d.x : 0 });
			ymin = d3.min(data, function(d){ return d.y < 0 ? d.y : 0 });
		}

		opts.margin = opts.margin || {top: 8, right: 10, bottom: 40, left: 45};
		opts.label = opts.label || {};
		opts.label.xaxis = opts.label.xaxis || 'x axis';
		opts.label.yaxis = opts.label.yaxis || 'y axis';
		opts.domain = opts.domain || {};
		opts.domain.x = opts.domain.x || [xmin,1];
		opts.domain.y = opts.domain.y || [ymin,1];

		displayScatterGraph.opts = opts;

		var svgh = parseInt(d3.select(id).style('height')),
			svgw = parseInt(d3.select(id).style('width')),
		margin = opts.margin;


	var width = svgw - margin.left - margin.right,
		height = svgh - margin.top - margin.bottom,
			container = d3.select(id);

		// Create svg if it does not exist
		container.selectAll('svg').data([0])
			.enter().append("svg:svg")
						.attr("width", "100%")
						.attr("height", "100%");
		var graph = container.select('svg');

	var x = d3.scale.linear()
		.range([0, width])
		.domain(opts.domain.x);
	var y = d3.scale.linear()
		.range([height, 0])
		.domain(opts.domain.y);

	// Append circle group if 
	graph.selectAll("g.box").data([0])
		.enter().append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			.attr("class", "box")

	graph.selectAll("g.trendbox").data([0])
		.enter().append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			.attr("class", "trendbox")

	var g = graph.select("g.box");

		var c = g.selectAll("circle")
		.data(data, function(d){return d.i});
	
	c.enter()
		.append("circle")
			.attr("r", 0)
			.attr("cx", function(d) {return x(d.x)})
			.attr("cy", function(d) {return y(d.y)})
			.transition().duration(750)
			.attr("r", 3)

	c.attr("cx", function(d) {return x(d.x)})
			.attr("cy", function(d) {return y(d.y)})

	c.exit().transition().duration(750)
		.attr("r", 0)
		.remove();

	// Only plot trendline when having more than n datapoints
	if(data.length > 9)
	{
		// get the x and y values for least squares
		var xSeries = data.map(function(d) { return parseFloat(d.x); });
		var ySeries = data.map(function(d) { return parseFloat(d.y); });
		
		var lsq = leastSquares(xSeries, ySeries);

		if(lsq.fit)
		{
			var x1 = opts.domain.x[0];
			var y1 = lsq.slope * x1 + lsq.intercept;
			var x2 = opts.domain.x[1];
			var y2 = lsq.slope * x2 + lsq.intercept;

			// Contain trendline within domain bounds
			if(y1 < opts.domain.y[0])
			{
				x1 = -lsq.intercept/lsq.slope;
				y1 = 0;
			}
			else if(y1 > opts.domain.y[1])
			{
				y1 = opts.domain.y[1];
				x1 = (y1 - lsq.intercept)/lsq.slope;
			}

			if(y2 < opts.domain.y[0])
			{
				x2 = -lsq.intercept/lsq.slope;
				y2 = 0;
			}
			else if(y2 > opts.domain.y[1])
			{
				y2 = opts.domain.y[1];
				x2 = (y2 - lsq.intercept)/lsq.slope;
			}

			// apply the results of the least squares regression
			var trendData = [[x1,y1,x2,y2]];
	
		}
		else
		{
			var trendData = [],
			lsq = {rsq: 0},
			x2 = -5;
		}
		
	}
	else
	{
		var trendData = [],
			lsq = {rsq: 0},
			x2 = -5;
	}
	
	var t = graph.select("g.trendbox");

	var trendline = t.selectAll(".trendline")
		.data(trendData);
		
	var t_enter = trendline.enter();
		
	t_enter.append("line")
		.attr("class", "trendline")
		.attr("stroke-width", 1);

	t_enter.append("text")
		.attr("class", "trendline-label")

	// Update trendline
	t.selectAll('.trendline')
		.transition().duration(750)
			.style("opacity", function(){return x2 == -5 ? 0 : 1})
			.attr("x1", function(d) { return x(d[0]); })
			.attr("y1", function(d) { return y(d[1]); })
			.attr("x2", function(d) { return x(d[2]); })
			.attr("y2", function(d) { return y(d[3]); })

	// display r-square on the chart
	var decimalFormat = d3.format("0.2f");
	t.selectAll(".trendline-label")
		.transition().duration(750)
		.text("r-sq: " + decimalFormat(lsq.rsq))
		.style("opacity", function(){return x2 == -5 ? 0 : 1})
		.attr("x", x(opts.domain.x[1]))
		.attr("y", 0);
	
		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("bottom")
			.ticks(Math.max(width/50, 2));
	graph.selectAll('.x.axis').data([0])
			.enter().append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(" + margin.left + "," + (height + margin.top) + ")")
			.call(xAxis);

		var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.ticks(Math.max(height/35, 2));
	graph.selectAll('.y.axis').data([0])
			.enter().append("g")
			.attr("class", "y axis")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			.call(yAxis);

	// Xaxis label
	graph.selectAll('.x.label').data([0])
		.enter().append("text")
				.attr("class", "x label")
				.attr("x", (width + margin.left + margin.right) / 2)
				.attr("y", height + margin.bottom + margin.top - 7)
				.text(opts.label.xaxis);

	// Yaxis label
	graph.selectAll('.y.label').data([0])
		.enter().append("text")
				.attr("class", "y label")
				.attr("text-anchor", "center")
				.attr("x", height / -2)
				.attr("y", 5)
				.attr("dy", ".75em")
				.attr("transform", "rotate(-90)")
				.text(opts.label.yaxis);

	return graph;
}

function displayHexbin1Graph(id, data, opts)
{
		data = data || [];
		opts = opts || displayScatterGraph.opts || {};

		xmin = 0;
		ymin = 0;

		// Get minimum xvalue
		if(data.length)
		{
			xmin = d3.min(data, function(d){ return d.x < 0 ? d.x : 0 });
			ymin = d3.min(data, function(d){ return d.y < 0 ? d.y : 0 });
		}

		opts.margin = opts.margin || {top: 8, right: 10, bottom: 40, left: 45};
		opts.label = opts.label || {};
		opts.label.xaxis = opts.label.xaxis || 'x axis';
		opts.label.yaxis = opts.label.yaxis || 'y axis';
		opts.domain = opts.domain || {};
		opts.domain.x = opts.domain.x || [xmin,1];
		opts.domain.y = opts.domain.y || [ymin,1];

		displayScatterGraph.opts = opts;

		var svgh = parseInt(d3.select(id).style('height')),
			svgw = parseInt(d3.select(id).style('width')),
		margin = opts.margin;


	var width = svgw - margin.left - margin.right,
		height = svgh - margin.top - margin.bottom,
			container = d3.select(id);

		// Create svg if it does not exist
		container.selectAll('svg').data([0])
			.enter().append("svg:svg")
						.attr("width", "100%")
						.attr("height", "100%");
		var graph = container.select('svg');

	var x = d3.scale.linear()
		.range([0, width])
		.domain(opts.domain.x);
	var y = d3.scale.linear()
		.range([height, 0])
		.domain(opts.domain.y);

	// Append circle group if 
	graph.selectAll("g.box").data([0])
		.enter().append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			.attr("class", "box")

	graph.selectAll("g.trendbox").data([0])
		.enter().append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			.attr("class", "trendbox")

	var g = graph.select("g.box");

		var c = g.selectAll("circle")
		.data(data, function(d){return d.i});
	
	c.enter()
		.append("circle")
			.attr("r", 0)
			.attr("cx", function(d) {return x(d.x)})
			.attr("cy", function(d) {return y(d.y)})
			.transition().duration(750)
			.attr("r", 3)

	c.attr("cx", function(d) {return x(d.x)})
			.attr("cy", function(d) {return y(d.y)})

	c.exit().transition().duration(750)
		.attr("r", 0)
		.remove();

	// Only plot trendline when having more than n datapoints
	if(data.length > 9)
	{
		// get the x and y values for least squares
		var xSeries = data.map(function(d) { return parseFloat(d.x); });
		var ySeries = data.map(function(d) { return parseFloat(d.y); });
		
		var lsq = leastSquares(xSeries, ySeries);

		if(lsq.fit)
		{
			var x1 = opts.domain.x[0];
			var y1 = lsq.slope * x1 + lsq.intercept;
			var x2 = opts.domain.x[1];
			var y2 = lsq.slope * x2 + lsq.intercept;

			// Contain trendline within domain bounds
			if(y1 < opts.domain.y[0])
			{
				x1 = -lsq.intercept/lsq.slope;
				y1 = 0;
			}
			else if(y1 > opts.domain.y[1])
			{
				y1 = opts.domain.y[1];
				x1 = (y1 - lsq.intercept)/lsq.slope;
			}

			if(y2 < opts.domain.y[0])
			{
				x2 = -lsq.intercept/lsq.slope;
				y2 = 0;
			}
			else if(y2 > opts.domain.y[1])
			{
				y2 = opts.domain.y[1];
				x2 = (y2 - lsq.intercept)/lsq.slope;
			}

			// apply the results of the least squares regression
			var trendData = [[x1,y1,x2,y2]];
	
		}
		else
		{
			var trendData = [],
			lsq = {rsq: 0},
			x2 = -5;
		}
		
	}
	else
	{
		var trendData = [],
			lsq = {rsq: 0},
			x2 = -5;
	}
	
	var t = graph.select("g.trendbox");

	var trendline = t.selectAll(".trendline")
		.data(trendData);
		
	var t_enter = trendline.enter();
		
	t_enter.append("line")
		.attr("class", "trendline")
		.attr("stroke-width", 1);

	t_enter.append("text")
		.attr("class", "trendline-label")

	// Update trendline
	t.selectAll('.trendline')
		.transition().duration(750)
			.style("opacity", function(){return x2 == -5 ? 0 : 1})
			.attr("x1", function(d) { return x(d[0]); })
			.attr("y1", function(d) { return y(d[1]); })
			.attr("x2", function(d) { return x(d[2]); })
			.attr("y2", function(d) { return y(d[3]); })

	// display r-square on the chart
	var decimalFormat = d3.format("0.2f");
	t.selectAll(".trendline-label")
		.transition().duration(750)
		.text("r-sq: " + decimalFormat(lsq.rsq))
		.style("opacity", function(){return x2 == -5 ? 0 : 1})
		.attr("x", x(opts.domain.x[1]))
		.attr("y", 0);
	
		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("bottom")
			.ticks(Math.max(width/50, 2));
	graph.selectAll('.x.axis').data([0])
			.enter().append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(" + margin.left + "," + (height + margin.top) + ")")
			.call(xAxis);

		var yAxis = d3.svg.axis()
			.scale(y)
			.orient("left")
			.ticks(Math.max(height/35, 2));
	graph.selectAll('.y.axis').data([0])
			.enter().append("g")
			.attr("class", "y axis")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			.call(yAxis);

	// Xaxis label
	graph.selectAll('.x.label').data([0])
		.enter().append("text")
				.attr("class", "x label")
				.attr("x", (width + margin.left + margin.right) / 2)
				.attr("y", height + margin.bottom + margin.top - 7)
				.text(opts.label.xaxis);

	// Yaxis label
	graph.selectAll('.y.label').data([0])
		.enter().append("text")
				.attr("class", "y label")
				.attr("text-anchor", "center")
				.attr("x", height / -2)
				.attr("y", 5)
				.attr("dy", ".75em")
				.attr("transform", "rotate(-90)")
				.text(opts.label.yaxis);

	return graph;
}