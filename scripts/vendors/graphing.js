/// DISPLAY SCATTER GRAPH ///

	function displayScatterGraph(id, data, opts)
	{
    	data = data || [];
    	opts = opts || displayScatterGraph.opts || {};

    	xmin = 0;
    	ymin = 0;

    	// Get minimum xvalue
    	if(data.length)
    	{
    		xmin = d3.min(data, function(d){ return d.x < 0 ? d.x : 0 });
    		ymin = d3.min(data, function(d){ return d.y < 0 ? d.y : 0 });
    	}



    	opts.title = opts.title || '';
    	opts.margin = opts.margin || {top: 8, right: 10, bottom: 40, left: 45};
    	opts.label = opts.label || {};
    	opts.label.xaxis = opts.label.xaxis || 'x axis';
    	opts.label.yaxis = opts.label.yaxis || 'y axis';
    	opts.domain = opts.domain || {};
    	opts.domain.x = opts.domain.x || [xmin,1];
    	opts.domain.y = opts.domain.y || [ymin,1];

    	displayScatterGraph.opts = opts;

    	var svgh = parseInt(d3.select(id).style('height')),
    		svgw = parseInt(d3.select(id).style('width')),
			margin = opts.margin;


		var	width = svgw - margin.left - margin.right,
			height = svgh - margin.top - margin.bottom,
    		container = d3.select(id);

    	// Create svg if it does not exist
	    container.selectAll('svg').data([0])
	    	.enter().append("svg:svg")
    					.attr("width", "100%")
    					.attr("height", "100%");
    	var graph = container.select('svg');

		var x = d3.scale.linear()
			.range([0, width])
			.domain(opts.domain.x);
		var y = d3.scale.linear()
			.range([height, 0])
			.domain(opts.domain.y);

		// Append circle group if 
		graph.selectAll("g.box").data([0])
			.enter().append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
				.attr("class", "box")

		graph.selectAll("g.trendbox").data([0])
			.enter().append("g")
				.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
				.attr("class", "trendbox")

		var g = graph.select("g.box");

    	var c = g.selectAll("circle")
			.data(data, function(d){return d.i});
		
		c.enter()
			.append("circle")
				.attr("r", 0)
				.attr("cx", function(d) {return x(d.x)})
				.attr("cy", function(d) {return y(d.y)})
				.transition().duration(750)
				.attr("r", 3)

		c.attr("cx", function(d) {return x(d.x)})
				.attr("cy", function(d) {return y(d.y)})

		c.exit().transition().duration(750)
			.attr("r", 0)
			.remove();

		// Only plot trendline when having more than n datapoints
		if(data.length > 9)
		{
			// get the x and y values for least squares
			var xSeries = data.map(function(d) { return parseFloat(d.x); });
			var ySeries = data.map(function(d) { return parseFloat(d.y); });
			
			var lsq = leastSquares(xSeries, ySeries);

			if(lsq.fit)
			{
				var x1 = opts.domain.x[0];
				var y1 = lsq.slope * x1 + lsq.intercept;
				var x2 = opts.domain.x[1];
				var y2 = lsq.slope * x2 + lsq.intercept;

				// Contain trendline within domain bounds
				if(y1 < opts.domain.y[0])
				{
					x1 = -lsq.intercept/lsq.slope;
					y1 = 0;
				}
				else if(y1 > opts.domain.y[1])
				{
					y1 = opts.domain.y[1];
					x1 = (y1 - lsq.intercept)/lsq.slope;
				}

				if(y2 < opts.domain.y[0])
				{
					x2 = -lsq.intercept/lsq.slope;
					y2 = 0;
				}
				else if(y2 > opts.domain.y[1])
				{
					y2 = opts.domain.y[1];
					x2 = (y2 - lsq.intercept)/lsq.slope;
				}

				// apply the results of the least squares regression
				var trendData = [[x1,y1,x2,y2]];
		
			}
			else
			{
				var trendData = [],
				lsq = {rsq: 0},
				x2 = -5;
			}
			
		}
		else
		{
			var trendData = [],
				lsq = {rsq: 0},
				x2 = -5;
		}
		
		var t = graph.select("g.trendbox");

		var trendline = t.selectAll(".trendline")
			.data(trendData);
			
		var t_enter = trendline.enter();
			
		t_enter.append("line")
			.attr("class", "trendline")
			.attr("stroke-width", 1);

		t_enter.append("text")
			.attr("class", "trendline-label")

		// Update trendline
		t.selectAll('.trendline')
			.transition().duration(750)
				.style("opacity", function(){return x2 == -5 ? 0 : 1})
				.attr("x1", function(d) { return x(d[0]); })
				.attr("y1", function(d) { return y(d[1]); })
				.attr("x2", function(d) { return x(d[2]); })
				.attr("y2", function(d) { return y(d[3]); })

		// display r-square on the chart
		var decimalFormat = d3.format("0.2f");
		t.selectAll(".trendline-label")
			.transition().duration(750)
			.text("r-sq: " + decimalFormat(lsq.rsq))
			.style("opacity", function(){return x2 == -5 ? 0 : 1})
			.attr("x", x(opts.domain.x[1]))
			.attr("y", 0);
		
	    var xAxis = d3.svg.axis()
	    	.scale(x)
	    	.orient("bottom")
	    	.ticks(Math.max(width/50, 2));
		graph.selectAll('.x.axis').data([0])
	    	.enter().append("g")
			  .attr("class", "x axis")
			  .attr("transform", "translate(" + margin.left + "," + (height + margin.top) + ")")
			  .call(xAxis);

	    var yAxis = d3.svg.axis()
	    	.scale(y)
	    	.orient("left")
	    	.ticks(Math.max(height/35, 2));
		graph.selectAll('.y.axis').data([0])
	    	.enter().append("g")
			  .attr("class", "y axis")
			  .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			  .call(yAxis);

    	// Add title
    	graph.selectAll('.title').data([0])
			.enter().append("text")
			    .attr("class", "title")
			    .attr("text-anchor", "center")
			    .attr("x", (width + margin.left + margin.right) / 2)
			    .attr("dy", ".75em")
			    .text(opts.title);

		// Xaxis label
		graph.selectAll('.x.label').data([0])
			.enter().append("text")
			    .attr("class", "x label")
			    .attr("x", (width + margin.left + margin.right) / 2)
			    .attr("y", height + margin.bottom + margin.top - 7)
			    .text(opts.label.xaxis);

		// Yaxis label
		graph.selectAll('.y.label').data([0])
			.enter().append("text")
			    .attr("class", "y label")
			    .attr("text-anchor", "center")
			    .attr("x", height / -2)
			    .attr("y", 5)
			    .attr("dy", ".75em")
			    .attr("transform", "rotate(-90)")
			    .text(opts.label.yaxis);


		// Update title
		graph.selectAll('.title')
				.transition().duration(750)
			    .attr("x", (width + margin.left + margin.right) / 2)
		if(opts.title)
		{
			graph.select(".title").text(opts.title);
		}

		return graph;
	}



/// DISPLAY BAR GRAPH ///

	function displayBarGraph(id, data, opts)
	{
    	// Get to the container object
    	var container = d3.select(id);

    	if(container.empty()) return;

    	opts = opts || container.select('div.options').datum();

    	data = data || [];

    	// Get minimum xvalue
    	if(data.length)
    	{
    		var xmin = d3.min(data, function(d){ return d.value < 0 ? d.value - d.se : 0 });
    		var xmax = d3.max(data, function(d){ return d.value + d.se > 1 ? d.value + d.se : 1 });
    		opts.xdomain = [xmin, xmax];
    	}

    	
    	// Init options
    	opts.margin = opts.margin || {top: 30, right: 10, bottom: 20, left: 35};
    	opts.colors = opts.colors || ["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"];
    	opts.xdomain = opts.xdomain || [0,1];
    	opts.tofixed = opts.tofixed == undefined ? 2 : opts.tofixed;

    	 // Handle resize
	    opts.registeredForResize = opts.registeredForResize || false;
	    var graphResize = 0;

	    if( ! opts.registeredForResize)
	    {
			
			$(window).resize(function(e) {
	
				graphResize = setTimeout(function(){displayBarGraph(id)}, 200);

			});

			opts.registeredForResize = true;
	    }
	    


    	// Create options store
    	container.selectAll('div.options').data([0])
	    	.enter().append("div")
    			.attr("class", "options")

    	var svgh = parseInt(d3.select(id).style('height'), 10),
    		svgw = parseInt(d3.select(id).style('width'), 10),
			margin = opts.margin;

		var	width = svgw - margin.left - margin.right,
			height = svgh - margin.top - margin.bottom
		

		if(opts.barHeight)
		{
			height = opts.barHeight * data.length;
			svgh = height + margin.top + margin.bottom
		}
		else
		{
			svgh = "100%"
		}
			

    	opts.barHeight = opts.barHeight || height / data.length;

    	var color = d3.scale.ordinal()
		    .range(opts.colors);

    	// Store options
    	container.select('div.options').data([opts]);

    	// Create svg if it does not exist
	    container.selectAll('svg').data([0])
	    	.enter().append("svg:svg")
    					.attr("width", "100%")
    					.attr("height", svgh);
    	var graph = container.select('svg');


    	// xAxis
		var x = d3.scale.linear()
			.range([0, width])
			.domain(opts.xdomain);
	    var xAxis = d3.svg.axis()
	    	.scale(x)
	    	.orient("bottom")
	    	.ticks(Math.max(width/50, 2));
		graph.selectAll('.x.axis').data([0])
	    	.enter().append("g")
			  .attr("class", "x axis")
			  .attr("transform", "translate(" + margin.left + "," + (height + margin.top) + ")")
			  .call(xAxis);

		graph.selectAll('.x.axis').transition().duration(750).call(xAxis);

		// Grid
		xAxis.tickSize(height)
			.tickFormat('')
		graph.selectAll('.x.grid').data([0])
	    	.enter().append("g")
			  .attr("class", "x grid")
  			  .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			  .call(xAxis);
		graph.selectAll('.x.grid').transition().duration(750).call(xAxis);

		// Bar group
		var bars = graph.selectAll(".bar")
		    .data(data);

		var g = bars.enter().append("g")
			.attr("class", "bar")
		    .attr("transform", function(d, i) { return "translate(" + margin.left + "," + (i * opts.barHeight + margin.top) + ")"; })
		
		// Bar area
		g.append("rect")
		    .attr("width", 0) // Initial width = 0
		    .attr("x", x(0))
		    .attr("class", "area")
		    .attr("height", opts.barHeight - 1)
		    .attr("rx", 2)
			.attr("ry", 2)
		    .style("fill", function(d) { return color(d.name); });

		// Error bar
		g.append("rect")
		    .attr("width", 0) // Initial width = 0
		    .attr("x", x(0))
		    .attr("y", opts.barHeight / 2 - 1)
		    .attr("class", "se")
		    .attr("height", 1);

		// Text
		g.append("text")
		    .attr("x", x(0)) // Initial width = 0
		    .attr("y", opts.barHeight / 2)
		    .attr("dy", ".35em");

		// yAxis
		var y = d3.scale.ordinal()
					.rangeRoundBands([0, height])
					.domain(data.map(function(d) { return d.name; }));
	    var yAxis = d3.svg.axis().scale(y).orient("left");
		graph.selectAll('.y.axis').data([0])
	    	.enter().append("g")
			  .attr("class", "y axis")
			  .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			  .call(yAxis);

		// Add title
    	graph.selectAll('.title').data([0])
			.enter().append("text")
			    .attr("class", "title")
			    .attr("text-anchor", "center")
			    .attr("dy", ".75em")
			    .text(opts.title);


		// Update title
		graph.selectAll('.title')
				.transition().duration(750)
			    .attr("x", (width + margin.left + margin.right) / 2)
		if(opts.title)
		{
			graph.select(".title").text(opts.title);
		}

		t = bars.transition().duration(750);

		// Update bar area rect
		t.select(".area")
			.attr("width", function(d){ return Math.abs(x(d.value) - x(0))})
			.attr("x", function(d) { return d.value < 0 ? x(d.value) : x(0) })


		// Update Error bar rect
		t.select(".se")
			.attr("width", function(d){ return Math.abs(x(d.se) - x(0))})
			.attr("x", function(d) { return d.value < 0 ? x(d.value) - (x(d.se) - x(0)) : x(d.value); })

		// Update text
		t.select("text")
			.attr("x", function(d) { return d.value < 0 ? x(d.value) + 27 : x(d.value) -3 })
	    	.text(function(d) { return d.value.toFixed(opts.tofixed); });

	    // On exit update (we can be called without data)
	    t = bars.exit().transition().duration(750);

		// Update bar area rect
		t.select(".area")
			.attr("width", function(d){ return Math.abs(x(d.value) - x(0))})
			.attr("x", function(d) { return d.value < 0 ? x(d.value) : x(0) })


		// Update Error bar rect
		t.select(".se")
			.attr("width", function(d){ return Math.abs(x(d.se) - x(0))})
			.attr("x", function(d) { return d.value < 0 ? x(d.value) - (x(d.se) - x(0)) : x(d.value); })

		// Update text
		t.select("text")
			.attr("x", function(d) { return d.value < 0 ? x(d.value) + 27 : x(d.value) -3 })
	    	.text(function(d) { return d.value.toFixed(opts.tofixed); });


		return graph;
	}


	function randomList(length, max)
	{
		var arr = [];
		for (var i = 0, l = length; i < l; i++) {
		    arr.push(Math.round(Math.random() * max))
		}
		return arr;
	}

/// TABULATE ///

function tabulate(id, data, columns) {

    var container = d3.select(id);

    // Create table if it does not exist
    container.selectAll('table').data([0])
    	.enter().append('table')
    		.attr("class", "table table-striped table-condensed");
   	var table = container.select("table");

   	table.selectAll('thead').data([0]).enter().append('thead');
	var thead = table.select('thead');

	table.selectAll('tbody').data([0]).enter().append('tbody');
	var tbody = table.select('tbody');

	thead.selectAll('tr').data([0])
		.enter().append('tr')
			.selectAll("th")
		        .data(columns)
		        .enter()
		        .append("th")
        	        .attr("style", function(d){return "text-align:" + d.align})
        	        .append("span")
	        	        .attr("tooltip-html-unsafe", function(d){return d.title != undefined ? d.title : ''})
	        	        .attr("style", function(d){return d.display != undefined ? 'display: inline' : 'display: none'})
	        	        .attr("class", "btn btn-default btn-xs")
			            .text(function(d) { return d.display; });

   
    // create a row for each object in the data
    var rows = tbody.selectAll("tr")
        .data(data, function(d) { return d.name; })
    
    rows.enter()
        	.append("tr");

    // create a cell in each row for each column
    var cells = rows.selectAll("td")
        .data(function(row) { 
            return columns.map(function(column) {

            	// Show N.A. for estimates if n_pairs = 0
            	if(column.fixed && row.n_pairs < 1)
            	{
            		return {
	                	name: column.name,
	                	value: 'N.A.',
	                	fixed: 0,
	                	align: column.align || "center",
	                	missing: column.missing || ''
            		}
            	}

                return {
                	name: column.name,
                	value: row[column.name],
                	fixed: column.fixed || 0,
                	align: column.align || "left",
	                missing: column.missing || ''
                };
            });
        })
    
    cells.enter()
        .append("td")
        .attr("style", function(d){return "text-align:" + d.align})

    // Update cells
    cells.text(function(d, i){ 
    	if(d.missing === d.value) return 'N.A.';
    	if(d.fixed) return d.value.toFixed(d.fixed); 
    	return d.value
    });
    
    return table;
}

function truncate(str, maxLength, suffix) {
	if(str.length > maxLength) {
		str = str.substring(0, maxLength + 1); 
		str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
		str = str + suffix;
	}
	return str;
}

/// DISPLAY PUNCHCARD ///

function punchcard(id, data, opts)
{
	var svgh = parseInt(d3.select(id).style('height')),
		svgw = parseInt(d3.select(id).style('width')),
		margin = {top: 40, right: 220, bottom: 20, left: 20},
		width = svgw - margin.left - margin.right,
		height = svgh - margin.top - margin.bottom,
		barHeight = 20;

	var dobj = new Date()
	var start_year = 1958,
		end_year = dobj.getFullYear();

	data.sort(function(a, b){
		return d3.descending(a.total, b.total); 
	})

	// Color range
	var c = d3.scale.category20c();

	// X Axis
	var x = d3.scale.linear()
		.range([0, width])
		.domain([start_year, end_year]);

	var xAxis = d3.svg.axis()
		.tickFormat(d3.format("0000"))
		.scale(x)
		.ticks(Math.max(width/50, 2))
		.orient("bottom");

	var svghead = d3.select(id).append("svg");

	svghead.attr("width", width + margin.left + margin.right)
		.attr('height', 20)
		.attr('style', 'position: fixed');

	svghead.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate("+margin.left+"," + 0 + ")")
		.call(xAxis);

	var svg = d3.select(id).append("svg")

	svg.attr("width", width + margin.left + margin.right)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
	
	var items = data.length;

	svg.attr("height", items * 20 + margin.top + margin.bottom);

	// Line group
	var lines = svg.selectAll(".item")
	    .data(data);

	var g = lines.enter().append("g")
		.attr("class", "item")
	    .attr("transform", function(d, i) { return "translate(" + margin.left + "," + (i * barHeight + margin.top) + ")"; })
		.on("mouseover", mouseover)
		.on("mouseout", mouseout)

	var rScale = d3.scale.linear()
		.range([2, 9]);

	var circles = g.selectAll("circle")
		.data(function(d){return d.articles})
		.enter().append("circle");


	circles
		.attr("fill", function(d, i, j){ return c(j) })
		.attr("cx", function(d){return x(d[0]) })
		.attr("cy", -5)
		.attr("r", function(d) { 
					rScale.domain([0,this.parentNode.__data__.max]); 
					return rScale(d[1])
				});

	var numbers = g.selectAll(".value")
		.data(function(d){return d.articles})
		.enter()
		.append("text")

	numbers
		.style("fill", function(d, i, j){ return c(j) })
		.attr("x", function(d){return x(d[0]) })
		.attr("class","value")
	    .attr("text-anchor", "middle")
		.text(function(d){ return d[1] })
		.style("display","none");


	g.append("text")
		.attr("x", margin.left + width)
		.attr("class","label")
		.text(function(d){return truncate(d.name+" ("+d.total+")",30,"...")})
		.style("fill", function(d, i) { return c(i) })
		;


	function mouseover(p) {
		var g = d3.select(this).node();
		d3.select(g).selectAll("circle").style("display","none");
		d3.select(g).selectAll("text.value").style("display","block");
	}

	function mouseout(p) {
		var g = d3.select(this).node();
		d3.select(g).selectAll("circle").style("display","block");
		d3.select(g).selectAll("text.value").style("display","none");
	}

}

/// DISPLAY PUNCHCARD II ///

function punchcard_2(id, data, opts)
{
	// Get to the container object
	var container = d3.select(id);

	opts = opts || {};

	opts.sortcol = opts.sortcol || 0;
	opts.hide = opts.hide || [];
	opts.colors = opts.colors || ['rgb(8,48,107)','rgb(8,81,156)','rgb(33,113,181)','rgb(66,146,198)','rgb(107,174,214)','rgb(158,202,225)','rgb(198,219,239)','rgb(222,235,247)','rgb(247,251,255)'];

	var svgh = parseInt(d3.select(id).style('height')),
		svgw = parseInt(d3.select(id).style('width')),
		margin = {top: 40, right: 20, bottom: 20, left: 220},
		width = svgw - margin.left - margin.right,
		height = svgh - margin.top - margin.bottom,
		barHeight = 20;

	data.sort(function(a, b){
		return d3.descending(a.items[opts.sortcol].value, b.items[opts.sortcol].value); 
	})

	// Color range
	var c = d3.scale.ordinal()
		    .range(opts.colors);

	// X Axis
	var x = d3.scale.linear()
		.range([0, width])
		.domain([-0.1, 1]);

	var xAxis = d3.svg.axis()
		.tickFormat(d3.format("0000"))
		.scale(x)
		.ticks(Math.max(width/50, 2))
		.orient("bottom");

	// Create svg header if it does not exist
    container.selectAll('svg.head').data([0])
    	.enter().append("svg:svg")
					.attr("width", "100%")
					.attr("height", "100%")
					.attr("class", "head");
	var svghead = container.select('svg.head');

	svghead.attr("width", width + margin.left + margin.right)
		.attr('height', 20)
		.attr('style', 'position: fixed');

	svghead.selectAll('.x.axis').data([0])
			.enter().append("g")
				.attr("class", "x axis")

	svghead.select('.x.axis')
		.attr("transform", "translate("+margin.left+"," + 0 + ")")
		.call(xAxis);

	// Create svg if it does not exist
    container.selectAll('svg.body').data([0])
    	.enter().append("svg:svg")
					.attr("class", "body");
	var svg = container.select('svg.body');

	svg.attr("width", width + margin.left + margin.right)

	var items = data.length;

	svg.attr("height", items * 20 + margin.top + margin.bottom);

	// Line group
	var lines = svg.selectAll(".item")
	    .data(data, function(d){return d.name});

	g = lines.enter().append("g")

	g.style("opacity", 0)
		.attr("class", "item")
		.attr("transform", function(d, i) { return "translate(" + margin.left + "," + (i * barHeight + margin.top) + ")"; })

	// Grid
	g.append("rect")
		.attr("y", barHeight / -4)
		.attr("width", width)
		.attr("height", 1)

	g.append("text")
		.attr("x", -10)
		.attr("class","label")
		.text(function(d){return d.name});


	// Move when updating
	lines.transition().duration(750)
		.attr("transform", function(d, i) { return "translate(" + margin.left + "," + (i * barHeight + margin.top) + ")"; })
		.style('opacity', 1)

	// Fade out when removing
	lines.exit().transition().duration(750)
		.style('opacity', 0).remove()

	var rScale = d3.scale.linear()
		.range([2, 9]);

	var circles = lines.selectAll("circle")
		.data(function(d){return d.items})

	// New circles
	circles.enter().append("circle")
		.attr("r", 0);


	circles
		.attr("fill", function(d, i, j){ return c(i) })
		.attr("cx", function(d){return x(d.value) })
		.attr("cy", barHeight / -4)
		.transition().duration(750)
		.attr("r", function(d){
			var r = d.n_entries > 0 ? 5 : 0;
			// Check if hidden
			r = opts.hide.indexOf(d.name) != -1 ? 0 : r;
			return r
		});

	// Standard error
	var se = lines.selectAll(".se")
		.data(function(d){return d.items})

	se.enter().append("rect")
		.attr("x", function(d) { return x(d.value); })
	    .attr("width", 0) // Initial width = 0
	    .attr("class", "se")
	    .attr("height", 1);

	se.attr("y", barHeight / -4 )
	    .transition().duration(750)
	    .attr("x", function(d) { 
	    	return opts.hide.indexOf(d.name) != -1 ? x(d.value) : x(d.value - d.se); 
	    })
	    .attr("width", function(d){ 
	    	return opts.hide.indexOf(d.name) != -1 ? 0 : Math.abs(x(d.se * 2) - x(0))
	    })


}

// PUNCHCARD 3 *******************************************

function punchcard_3(id, data, opts)
{
	// Get to the container object
	var container = d3.select(id);

	opts = opts || {};

	opts.sortcol = opts.sortcol || 0;
	opts.hide = opts.hide || [];
	opts.colors = opts.colors || ['rgb(8,48,107)','rgb(8,81,156)','rgb(33,113,181)','rgb(66,146,198)','rgb(107,174,214)','rgb(158,202,225)','rgb(198,219,239)','rgb(222,235,247)','rgb(247,251,255)'];

	var svgh = parseInt(d3.select(id).style('height')),
		svgw = parseInt(d3.select(id).style('width')),
		labelWidth = 140,
		margin = {top: 10, right: 10, bottom: 10, left: labelWidth + 15},
		width = svgw - margin.left - margin.right,
		height = svgh - margin.top - margin.bottom,
		barHeight = 16,
		lineMax = 10
		itemSpacing = 4;

	// Get visible items
	visibleItems = []
	for (var i = 0; i < data[0].items.length; i++)
	{
		if(opts.hide.indexOf(data[0].items[i].name) == -1)
		{
			visibleItems.push(data[0].items[i].name)
		}
	};

	data.sort(function(a, b){
		return d3.descending(a.items[opts.sortcol].value, b.items[opts.sortcol].value); 
	})

	// Get minimum xvalue
	if(data.length)
	{
		var xmin = d3.min(data, function(trait){ 
			return d3.min(trait.items, function(item){
				return item.value < 0 ? item.value - item.se : 0
			})
		});
		var xmax = d3.max(data, function(trait){ 
			return d3.max(trait.items, function(item){
				return item.value + item.se > 1 ? item.value + item.se : 1
			})
		});
		opts.xdomain = [xmin, xmax];

	}

	// Tooltip
	var tip = d3.tip()
		.attr('class', 'd3-tip')
		.html(function(d) { return d; })
		.offset([-10, 0])

	// Color range
	var c = d3.scale.ordinal()
		    .range(opts.colors);

	// X Axis
	var x = d3.scale.linear()
		.range([0, width])
		.domain(opts.xdomain);

	var xAxis = d3.svg.axis()
		.tickFormat(d3.format("0000"))
		.scale(x)
		.ticks(Math.max(width/50, 2))
		.orient("bottom");


	// Create svg if it does not exist
    container.selectAll('svg.body').data([0])
    	.enter().append("svg:svg")
					.attr("class", "body");

	var svg = container.select('svg.body');

	svg.attr("width", width + margin.left + margin.right)
		.call(tip)

	var items = data.length,
		visible = 8 - opts.hide.length;

	svg.attr("height", items * (barHeight * visible + itemSpacing) + margin.top + margin.bottom);

	// Line group
	var lines = svg.selectAll(".item")
	    .data(data, function(d){return d.name});

	lines.enter().append("g")
		.style("opacity", 0)
		.attr("class", "item")
		.attr("transform", function(d, i) { return "translate(" + margin.left + "," + (i * (barHeight * visible + itemSpacing) + margin.top) + ")"; })

	// Move when updating
	lines.transition().duration(750)
		.attr("transform", function(d, i) { return "translate(" + margin.left + "," + (i * (barHeight * visible + itemSpacing) + margin.top) + ")"; })
		.style('opacity', 1)

	// Fade out when removing
	lines.exit().transition().duration(750)
		.style('opacity', 0).remove()

	// Backgroung
	var bg = lines.selectAll(".bg")
		.data([0])

	bg.enter().append("rect")
		.attr("class", "bg")
		.attr("x", 0)
		.attr("width", width -5 )
		.style("fill", "#f8f8f8")

	bg.transition().duration(750)
		.attr("height", barHeight * visibleItems.length)

	// Label
	var label = lines.selectAll(".label")
		.data(function(d){return [d.name]})

	label.enter().append("text")
		.attr("dx", -5)
		.attr("y", "0.5em")
		.attr("dy", "0.35em")
		.attr("class","label")
		.text(function(d){return d})
		.call(shorten, labelWidth)
		.on('mouseover', tip.show)
		.on('mouseout', tip.hide)

	$('#content').scroll(function(){
		tip.hide
	})

	label
		.attr("y", barHeight * visible / 2)

	var rects = lines.selectAll(".bar")
		.data(function(d){return d.items})

	// New rects
	rects.enter().append("rect")
	    .attr("class", "bar")
		.attr("x", x(0))
		.attr("width", 0) // Initial width = 0
		.style("fill", function(d, i, j){ return c(i) })

	rects
		.transition().duration(750)
		.attr("y", function(d){return (visibleItems.indexOf(d.name) * barHeight)})
		.attr("x", function(d) { return d.value < 0 ? x(d.value) : x(0) })
		.attr("height", barHeight - 1)
		.attr("width", function(d){
			w = opts.hide.indexOf(d.name) != -1 ? 0 : Math.abs(x(d.value) - x(0));
			w = d.n_entries > 0 ? w : 0;
			return w;
		});


	// Values
	var values = lines.selectAll(".val")
		.data(function(d){return d.items})

	values.enter().append("text")
		.attr("x", x(0)) // Initial width = 0
		.attr("class", "val")
		.attr("dy", "0.35em");

	values
		.transition().duration(750)
		.attr("y", function(d){return (visibleItems.indexOf(d.name) * barHeight + barHeight/2)})
		.attr("x", function(d){
			w = opts.hide.indexOf(d.name) != -1 ? 0 : x(d.value);
			w = d.n_entries > 0 ? w : 0;
			return d.value < 0 ? w + 27 : w - 3;
		})
		.text(function(d){return visibleItems.indexOf(d.name) > -1 ? d.value.toFixed(2) : ''})

	// Standard error
	var se = lines.selectAll(".se")
		.data(function(d){return d.items})

	se.enter().append("rect")
		.attr("x", function(d) { return x(d.value); })
	    .attr("width", 0) // Initial width = 0
	    .attr("class", "se")
	    .attr("height", 1);

	se.transition().duration(750)
		.attr("y", function(d){return (visibleItems.indexOf(d.name) * barHeight + barHeight / 2 - 1)})
	    .attr("x", function(d) {
	    	var r = d.value < 0 ? x(d.value) - (x(d.se) - x(0)) : x(d.value);
	    	return opts.hide.indexOf(d.name) != -1 ? 0 : r; 
	    })
	    .attr("width", function(d){ 
	    	return opts.hide.indexOf(d.name) != -1 ? 0 : Math.abs(x(d.se) - x(0))
	    })

}



// returns slope, intercept and r-square of the line
function leastSquares(xSeries, ySeries) {
	var reduceSumFunc = function(prev, cur) { return prev + cur; };
	
	var xBar = xSeries.reduce(reduceSumFunc) * 1.0 / xSeries.length;
	var yBar = ySeries.reduce(reduceSumFunc) * 1.0 / ySeries.length;

	var ssXX = xSeries.map(function(d) { return Math.pow(d - xBar, 2); })
		.reduce(reduceSumFunc);
	
	var ssYY = ySeries.map(function(d) { return Math.pow(d - yBar, 2); })
		.reduce(reduceSumFunc);
		
	var ssXY = xSeries.map(function(d, i) { return (d - xBar) * (ySeries[i] - yBar); })
		.reduce(reduceSumFunc);
	
	if(ssXX)
	{
		var slope = ssXY / ssXX;
		var intercept = yBar - (xBar * slope);
		var rSquare = Math.pow(ssXY, 2) / (ssXX * ssYY);
		return {slope: slope, intercept: intercept, rsq: rSquare, fit: 1};
	}

	
	return {slope: 1, intercept: 1, rsq: 1, fit: 0};
}

// Text shortener
function shorten(text, width){
	text.each(function() {
		var text = d3.select(this),
			words = text.text().split(/\s+/).reverse(),
			line = []

		// Remove text
		text.text(null)

		while (word = words.pop()) {
			line.push(word);
			text.text(line.join(" ") + "…")
			if (text.node().getComputedTextLength() > width) {
				line.pop();
				text.text(line.join(" ") + "…");
				break;
			}
			text.text(line.join(" "));
		}
	});
}

// Text wrapper by Mike Bostock
function wrap(text, width, height) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        dx = parseFloat(text.attr("dx")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan")
        	.attr("x", 0)
        	.attr("dx", dx)
        	.attr("y", y)
        	.attr("dy", ++lineNumber * lineHeight + dy + "em")
        	.text(word);
      }
    }
    text.selectAll('tspan')
  });
}

