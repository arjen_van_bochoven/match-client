'use strict';

angular.module('app.controllers', [])

.factory "defaults", [ ->
    current = {level: 'none', trait:'All Traits', filename:''}
    return {
        #baseurl: 'http://db.ctglab.nl/index.php?/'
        baseurl: 'http://localhost:9090/index.php?/'
        #baseurl: 'http://h2376938.stratoserver.net/index.php?/'
        levels: [
            {value: 'level1', label: 'Domain'}
            {value: 'level2', label: 'ICF/ICD10 Main Chapter'}
            {value: 'level3', label: 'ICF/ICD10 Subchapter'}
        ]
        colorRange:
            blue : ['rgb(8,48,107)','rgb(8,81,156)','rgb(33,113,181)','rgb(66,146,198)','rgb(107,174,214)','rgb(158,202,225)','rgb(198,219,239)','rgb(222,235,247)','rgb(247,251,255)']
            red : ['rgb(103,0,13)','rgb(165,15,21)','rgb(203,24,29)','rgb(239,59,44)','rgb(251,106,74)','rgb(252,146,114)','rgb(252,187,161)','rgb(254,224,210)','rgb(255,245,240)']
            green : ['rgb(0,68,27)','rgb(0,109,44)','rgb(35,139,69)','rgb(65,171,93)','rgb(116,196,118)','rgb(161,217,155)','rgb(199,233,192)','rgb(229,245,224)','rgb(247,252,245)']
            ace : ['#88419d', '#8c6bb1','#8c96c6','#9ebcda','#238b45','#41ae76', '#66c2a4','#99d8c9']
            cor : ['#ef3b2c', '#fb6a4a','#fc9272','#08306b','#2171b5','#6baed6', '#c6dbef','#9e9ac8']
            head: ['rgb(213,62,79)','rgb(244,109,67)','rgb(253,174,97)','rgb(254,224,139)','rgb(230,245,152)','rgb(171,221,164)','rgb(102,194,165)','rgb(50,136,189)']
            blueOrange: ['rgb(32,24,21)','rgb(15,100,153)','rgb(74,181,248)','rgb(249,217,147)','rgb(247,179,130)','rgb(252,166,108)','rgb(247,106,13)','rgb(204,21,14)']
            summerChill: ['rgb(32,24,21)','rgb(22,179,59)','rgb(174,222,239)','rgb(240,254,255)','rgb(23,179,252)','rgb(4,60,81)']
            indianEarthy: ['rgb(32,24,21)','rgb(52,47,41)','rgb(61, 58, 36)','rgb(247,106,13)','rgb(161, 71, 39)','rgb(230, 209, 170)','rgb(88, 114, 101)']
            firenze: ['rgb(32,24,21)','rgb(67,1212,119)','rgb(250,208,111)','rgb(247,106,13)','rgb(204,21,14)','rgb(147,24,17)']
            retroup: ['rgb(255,255,255)','rgb(228,105,64)','rgb(255,221,167)','rgb(26,174,188)','rgb(186,36,48)']
            doglass: ['rgb(67,52,18)','rgb(129,95,44)','rgb(241,210,153)','rgb(248,161,6)','rgb(231,67,16)']
            yramirp: ['rgb(32,24,21)','rgb(255,43,7)','rgb(147,198,33)','rgb(0,180,207)']

        defaultEstimates : ['mzall', 'mzm', 'mzf', 'dzall', 'dzss', 'dzm', 'dzf', 'dos']
        defaultACE : ['h2_all', 'h2_ss', 'h2_m', 'h2_f', 'c2_all', 'c2_ss', 'c2_m', 'c2_f']
        lseLabels: ['2(mz-dz) all','2(mz-dz) ss','2(mz-dz) m','2(mz-dz) f','2dz-mz all','2dz-mz ss','2dz-mz m','2dz-mz f']
        columns : [
            {name:'name', align: 'left'}
            {name:'value', display: 'Est.', align: 'right', fixed: 3, title: 'Estimate obtained from<br>DerSimonian-Laird random effects<br>meta-analytical approach.'}
            {name:'se', display: 'SE', align: 'right', fixed: 3, title: 'Standard error of the estimate,<br>calculated in the random effects<br>meta-analysis, using the<br>DerSimonian-Laird approach.'}
            {name:'n_entries', display: 'Ntraits', align: 'right', title: 'Number of entries on which the<br>estimate is based.'}
            {name:'n_pairs', display: 'Npairs', align: 'right', title: 'Total number of dependent pairs<br>of twins included in the<br>meta-analysis. The pairs are<br>dependent as traits may have<br>been measured in<br>the same or overlapping datasets.'}
        ]
        sections: [
                {name:'Analysis', sub:'Specific Traits',link: '/specific', icon: 'bar-chart', color: 'danger'}
                {name:'Analysis', sub:'Multiple Traits',link: '/multiple', icon: 'bar-chart', color: 'warning'}
                {name:'Overview', sub:'What\'s in here',link: '/overview', icon: 'line-chart', color: 'success'}
                {name:'About', sub:'How it works',link: '/about', icon: 'info', color: 'primary'}
                {name:'Search', sub:'Find my Trait',link: '/overview/search'}
        ]
        current : ->
            return current
    }
]

.controller('downloadCSV', [
    '$scope'
    'Plot'
    'defaults'
    'utils'
    ($scope, $plot, $defaults, $utils) ->

        
        $scope.submit = ($event) ->
            # Inject data

            $('input[name="json"]').val(JSON.stringify($plot.data))

            # Get filename
            filename = $utils.dirify($defaults.current().filename)

            # Set action
            $event.target.action = $defaults.baseurl + 'download/csv/' + filename

            
            # Download file
            $event.target.submit()

])

.factory "utils", [ ->
    return {
        dirify: (s,d) ->
            if (!d)
                d = "_";
            s = s.replace(/<[^>]+>/g, '');
            s = s.toLowerCase();
            s = s.replace(/&[^;\s]+;/g, '');
            s = s.replace(/[^-a-z0-9_ ]/g, '');
            s = s.replace(/\s+/g, '_');
            s = s.replace(/_+$/, '');
            s = s.replace(/_+/g, d);
            return s;
    }
]

# overall control
.controller('AppCtrl', [
    '$scope', '$location'
    ($scope, $location) ->
        $scope.isSpecificPage = ->
            path = $location.path()
            return _.contains( ['/home','/404', '/pages/500', '/pages/login', '/pages/signin', '/pages/signin2', '/pages/signup', '/pages/signup1', '/pages/signup2', '/pages/lock-screen'], path )

        $scope.whichNav = ->
            path = $location.path()
            if ! path.indexOf '/specific/'
                return 'views/nav/specific_nav.html'

            if ! path.indexOf '/multiple/'
                return 'views/nav/multiple_nav.html'

            if ! path.indexOf '/overview/'
                return 'views/nav/overview_nav.html'


        $scope.showLevels = ->
            path = $location.path()

            # No levels on the search page
            if path == '/specific/search'
                return false

            return ! path.indexOf '/specific/'

        
        $scope.$on('$routeChangeStart', ->
            
            # Clear resize handlers on change page
            $(window).off("resize")

            # Pull up menu
            $('#app').removeClass('show-menu')
        )

        $scope.main =
            brand: 'MaTCH'

])

.controller('TopNavCtrl', [
    '$scope'
    'defaults'
    ($scope, $defaults) ->
        $scope.sections = $defaults.sections
])

.controller('NavBtnCtrl', [
    '$scope'
    'defaults'
    ($scope, $defaults) ->
        $scope.sections = $defaults.sections

        $scope.noSearch = (element) ->
          return element.name != 'Search'
])

.controller('LevelCtrl', [
    '$rootScope'
    '$scope'
    'Plot'
    'defaults'
    ($rootScope, $scope, $plot, $defaults) ->

        $scope.levelselects = [
            {name:'All Traits', level:'none'},
            {name:'Domain', level:'level1'},
            {name:'ICF/ICD10 Main Chapter', level:'level2'},
            {name:'ICF/ICD10 Subchapter', level:'level3'}
        ];

        $scope.levelselect = $scope.levelselects[0]

        promise = $plot.retrieveData("user/dropdowns", {})
        promise.then =>
            $scope.dbLevels = $plot.data
            $scope.resetOptions('level1', 'level2', 'level3')

        $scope.resetOptions = ->

            # Reset dropdowns
            angular.forEach(arguments, (model) ->
                $scope[model] = $scope.dbLevels[model][0]
            )

            $defaults.current().level = $scope.levelselect.level || 'none'
            if $scope.levelselect.level == 'none'
                $defaults.current().trait = 'All Traits'
            else
                $defaults.current().trait = $scope[$scope.levelselect.level].name

            # Signal we have a change
            args = {}
            if $scope.levelselect.level != 'none'
                args[$scope.levelselect.level] = $scope[$scope.levelselect.level].name;

            $rootScope.$broadcast('levels.changed', args)


])

.controller('NavCtrl', [
    '$scope', 'taskStorage', 'filterFilter'
    ($scope, taskStorage, filterFilter) ->
        # init
        tasks = $scope.tasks = taskStorage.get()
        $scope.taskRemainingCount = filterFilter(tasks, {completed: false}).length

        $scope.$on('taskRemaining:changed', (event, count) ->
            $scope.taskRemainingCount = count
        )
])

.controller('DashboardCtrl', [
    '$scope'
    ($scope) ->

])

